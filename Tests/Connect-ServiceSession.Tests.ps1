BeforeAll {
    Write-Host "Testing Connect-ServiceSession" -ForegroundColor Red -BackgroundColor Yellow
}

Describe "Connect-ServiceSession" {
    InModuleScope Office365 {
<#
        Context "There is no active session and connect with cert" {
            BeforeEach {
                mock Connect-Office365 -MockWith { return $true } -Verifiable
                mock Get-AzureADTenantDetail -MockWith { return $true } -Verifiable
                #mock Connect-AzureAD -MockWith { return $true }

                Connect-ServiceSession -AppID $global:app_id -Organization $global:tenant_id  -CertificateThumbprint $global:CertificateThumbprint

            }
            
            It "Should try to establish a new connection and prompt for credentials." {
                Should -InvokeVerifiable
            }
        }        

        Context "There is no active session" {
            BeforeEach {
                mock Connect-Office365 -MockWith { return $true } -Verifiable
                mock Get-AzureADTenantDetail -MockWith { return $true } -Verifiable
                mock Connect-AzureAD -MockWith { return $true }
                mock Get-Credential -MockWith { return $global:Credential } -Verifiable
               # Connect-ServiceSession
            }
            
            It "Should try to establish a new connection and prompt for credentials." {
              #  Should -InvokeVerifiable
            }
        }
       
        Context "There is an active session" {
            BeforeEach {
                mock Connect-Office365 -MockWith { return $true } -Verifiable
                mock Get-Credential -MockWith { return $global:Credential } -Verifiable
                $script:Office365Session = [PSCustomObject]@{
                    State = "Opened"
                }
                mock Get-AzureADTenantDetail { return $true } -Verifiable

                Connect-ServiceSession -Cr .\Tests\alexCreds.txt
            }

            It "makes an attempt to connect to AzureAD" {
                Should -InvokeVerifiable
            }
            AfterEach {
                Disconnect-ExchangeOnline -Confirm:$false
            }
        }
#>
        Context "There is no active connection with AzureAD" {
            BeforeEach {
                Disconnect-AzureAD
                $script:Office365Session = [PSCustomObject]@{
                    State = "Opened"
                }
                mock Connect-AzureAD { return $true } -Verifiable

                Connect-ServiceSession -Cr .\Tests\alexCreds.txt
            }
            It "Catches the AadNeedAuthenticationException and calls Connect-AzureAD" {
                Should -InvokeVerifiable
            }
        }
    }
}