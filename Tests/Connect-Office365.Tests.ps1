﻿BeforeAll {
    Write-Host "Testing Connect-Office365" -ForegroundColor Red -BackgroundColor Yellow
}

Describe "Connect-Office365" {
    InModuleScope Office365 {

        Context "There is no active session and connect with cert" {

            BeforeEach {
                Connect-Office365 -AppID $global:app_id -Organization $global:tenant_id  -CertificateThumbprint $global:CertificateThumbprint
            }
            
            It "Should try to establish a new connection." {
                Should -InvokeVerifiable
            }

            AfterEach {
                Disconnect-ExchangeOnline -Confirm:$false
            }
        }        


        Context "There is no active session and connect with user credentials" {

            BeforeEach {
                Connect-Office365 -Credential $global:Credential
            }
            
            It "Should try to establish a new connection using supplied user credential object." {
                Should -InvokeVerifiable
            }

            AfterEach {
                Disconnect-ExchangeOnline -Confirm:$false
            }
        }      

    }
}