BeforeAll {
    Write-Host "Testing Block-EmployeeAccess" -ForegroundColor Red -BackgroundColor Yellow
}

Describe "Block-EmployeeAccess" {
    InModuleScope Office365 {
        Context "There is no active session" {
          
            It "Should try to establish a new connection and prompt for credentials." {
                $cr = "c:\code\password.txt"
                Connect-ServiceSession -cr $cr
                mock -ModuleName Office365 Set-QuickCASMailbox {
                    return $null
                } -Verifiable
                mock -ModuleName Office365 Get-MobileDevice {
                    return $null
                } -Verifiable
                mock -ModuleName Office365 Clear-MobileDevice {
                    return $null
                } -Verifiable

                mock -ModuleName Office365 Connect-ServiceSession {
                    return $null
                } -Verifiable

                Block-EmployeeAccess -UserPrincipalName "bssadmin@everesttoys.com" -cr $cr
                Should -InvokeVerifiable
            }

        }
    }
}