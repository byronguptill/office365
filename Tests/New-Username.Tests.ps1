BeforeAll {
    Write-Host "New-Username" -ForegroundColor Red -BackgroundColor Yellow
}


Describe "New-Username" {
    InModuleScope Office365 {    
        Context "Username Exists" {
            BeforeEach {
                mock Connect-ServiceSession { return $true }
                mock Get-AzureADUser { return $true }                
            }
            It "attempts to find a unique user name by adding one character at a time to the given name." {
                {New-Username -GivenName "Ceasar" -Surname "Maximus"  -AppID $global:app_id -Organization $global:tenant_id -CertificateThumbprint $global:CertificateThumbprint} | Should -Throw "Could not determine username for the Ceasar Maximus"
            }            
        }
    
        Context "Active Office365 Session" {
            BeforeEach {
                mock Connect-ServiceSession { return $true }
                mock Get-AzureADUser { return $false }                
            }
            It "finds no existing string in Azure AD and return the given name as the username." {
                New-Username -GivenName "Ceasar" -Surname "Maximus" | Should -Be 'cmaximus'
            }
            It "Removes spaces in the last name." {
                New-Username -GivenName "Ceasar" -Surname "Von Maximus" | Should -Be 'cvonmaximus'
            }
        }
    }
}