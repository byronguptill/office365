properties {
    $script:scripts = New-Object -TypeName System.Collections.ArrayList
    $script:signed_scripts = New-Object -TypeName System.Collections.ArrayList
    $script:tokens = $PSScriptRoot.Split("`\")
    $script:module_root = $tokens[$tokens.length -1]
    $script:scratch_folder = "$env:TEMP\$module_root"
    $script:certificate = Resolve-path -path "C:\code\certificate.txt"
    $ConfigFile = "$PSSCriptRoot\Tests\new_hire_config.json"



    #write-verbose $script:scripts
    #write-verbose $script:signed_scripts
    #write-verbose $script:tokens
    write-verbose $script:module_root
    write-verbose $script:scratch_folder
    write-verbose $script:certificate
    write-verbose $ConfigFile


    $filestream = (Get-Content -Path $ConfigFile -Raw)
    $script:NewHireConfigurationVariables = [PSCustomObject](ConvertFrom-Json -InputObject $filestream)
    $ValidateCompany = ($script:NewHireConfigurationVariables.company).Split(",")
    $global:ValidCompany = @{}
    foreach ($temp in $ValidateCompany)
    {
        $tmp = $temp.Split(":")
        $global:ValidCompany[$tmp[0]] = $tmp[1]
    }

    $global:app_id = $script:NewHireConfigurationVariables.app_id 
    $global:tenant_id = $script:NewHireConfigurationVariables.tenant_id
    $global:CertificateThumbprint = $script:NewHireConfigurationVariables.CertificateThumbprint

    $password = ConvertTo-SecureString '7As0&KUrukdLtsqVc^CkD6waqGgEdD6BrZ32Bm0zV@u6Q25bgkYE@$1O7E@7@r&4Zk*Km%afR0Ox' -AsPlainText -Force
    $global:Credential = New-Object System.Management.Automation.PSCredential ('script_test@everesttoys.com', $password)
   
    Get-ChildItem -Path $PSScriptRoot\$script:module_root\Public  -Filter *-*.ps1  | ForEach-Object {[void]$scripts.Add($_.FullName) }
    Get-ChildItem -Path $PSScriptRoot\$script:module_root\Private  -Filter *-*.ps1  | ForEach-Object {[void]$scripts.Add($_.FullName) }

    Write-Verbose -Message "*********************************"
    Write-Verbose -Message "*********PSake Build*************"
    Write-Verbose -Message "Module root: $script:module_root"
    Write-Verbose -Message "Scratch folder: $script:scratch_folder"
    Write-Verbose -Message "Script root: $PSScriptRoot"
    Write-Verbose -Message "Certificate: $script:certificate"
    Write-Verbose -Message "Config file: $ConfigFile"
    #Write-Verbose -Message "$filestream"
    Write-Verbose -Message "New Hire config variables: $script:NewHireConfigurationVariables"
    #Write-Verbose -Message "$ValidateCompany"
    Write-Verbose -Message "*********************************"

}


task default -depends Analyze, Test

task Analyze {
    $saResults = New-Object -TypeName System.Collections.ArrayList
    foreach ($script in $script:scripts)
    {
        $saResults = Invoke-ScriptAnalyzer -Path $script -Severity @('Error'<#,'Warning'#>) -Recurse -Verbose:$false
        if ($saResults) {
            $saResults | Format-Table
            Write-Error -Message 'One or more Script Analyzer errors/warnings where found. Build cannot continue!'
        }
    }
}

task Test {

    git branch

    Get-PSSession | Remove-PSSession
    Remove-Module EveAdmin -Force -ErrorAction SilentlyContinue | Out-Null
    Remove-Module Office365 -Force -ErrorAction SilentlyContinue | Out-Null

    Write-Host "`n`n`n********** Construct environment **********"
    Write-host  $script:NewHireConfigurationVariables 
    Import-module "C:\code\Office365\Office365"
    Import-module "C:\code\EveAdmin\EveAdmin"
    Write-Verbose "`n`n`n********** End Construct environment **********"

    $testResults = Invoke-Pester -Path $PSScriptRoot -PassThru
    if ($testResults.FailedCount -gt 0) {
        $testResults | Format-List
        Write-Error -Message 'One or more Pester tests failed. Build cannot continue!'
    }
}

task Sign {
    <#
    get certificate
    create workspace
    copy module into workspace.
    sign scripts and 
    #>

    Write-Verbose "[Verbose]`tGrabbing certificate path."
    $securecontents = $(Get-Content -Path $certificate | ConvertTo-SecureString)
    $creds = New-Object System.Management.Automation.PSCredential("username", $securecontents)
    $certificate = get-childitem -Path $creds.GetNetworkCredential().password 
    
    if (Test-Path -Path $script:scratch_folder)
    {
        Remove-Item -Path $script:scratch_folder -Recurse -Force -Verbose
    }

    Write-Verbose "[Verbose]`t$certificate"
    $local:scratch = New-Item -ItemType "directory" -Name $script:module_root  -Path $env:TEMP
    Write-Verbose "[Verbose]`t$($local:scratch.GetType())"
    if ( $local:scratch.GetType() -ne [System.IO.DirectoryInfo] )
    {
        Write-Error -Message "One or more Script Analyzer errors/warnings where found. Build cannot continue!`n$($error[0])"
    }


    Copy-Item -Path $PSScriptRoot\$script:module_root\* -Destination $local:scratch.FullName -Recurse -Exclude *.json -Verbose:$VerbosePreference

    
    Get-ChildItem -Path $local:scratch.FullName  -Filter *.ps?1  | ForEach-Object {[void]$signed_scripts.Add($_.FullName) }
    Get-ChildItem -Path $local:scratch.FullName  -Filter *-*.ps1 -Recurse  | ForEach-Object {[void]$signed_scripts.Add($_.FullName) }

    $saResults = New-Object -TypeName System.Collections.ArrayList
    foreach( $signed_script in $signed_scripts)
    {
        Write-Verbose "[Verbose]`tSigning $($signed_script)"
        $saResults = Set-AuthenticodeSignature -Certificate $certificate -FilePath $signed_script -Verbose:$VerbosePreference
        if ($saResults.Status -ne "Valid") {
            $saResults | Format-Table
            Write-Error -Message 'One or more Script Analyzer errors/warnings where found. Build cannot continue!'
        }
    }

    $catalog = New-FileCatalog -Path $local:scratch.FullName -CatalogFilePath "$($local:scratch.FullName)\$module_root.cat" -Verbose:$VerbosePreference

    $saResults = New-Object -TypeName System.Collections.ArrayList
    Write-Verbose "[Verbose]`tSigning $($catalog.FullName)"
    $saResults = Set-AuthenticodeSignature -Certificate $certificate -FilePath $catalog.FullName -Verbose:$VerbosePreference
    if ($saResults.Status -ne "Valid") {
        $saResults | Format-Table
        Write-Error -Message 'One or more Script Analyzer errors/warnings where found. Build cannot continue!'
    }
}


task Deploy -depends Analyze, Test, Sign {
    Remove-Module EveAdmin -Force | Out-Null
    Remove-Module Office365 -Force | Out-Null    

    $module_profile_path = "$(split-path($PROFILE.CurrentUserAllHosts))\Modules\$script:module_root"
    
    if (Test-Path -Path $module_profile_path)
    {
        Remove-Item -Path $module_profile_path -Recurse -Force
    }

    $from = $script:scratch_folder
    #$from = "$PSScriptRoot\$script:module_root"
    $to = $module_profile_path

    Copy-Item -path $from -Destination $to -Recurse
    Remove-Item -Path $from -Recurse -Force

    write-output "We have moved the files from: $from `n To: $to"
    
}


task Publish -depends Analyze, Test, Sign, Deploy {
    Invoke-PSDeploy -Path "$PSScriptRoot\$script:module_root.psdeploy.ps1" -Force -Verbose:$VerbosePreference
}