﻿#Requires -Version 5.1  

[System.Management.Automation.Runspaces.PSSession]$script:Office365Session
[System.Management.Automation.PSCredential]$script:UserCredential
#$script:Logpath = "$env:ProgramData\NewHireLogs"
$script:Logpath = "c:\NewHireLogs"
$script:LogFile =  "$Logpath\NEWHIRE-$(get-date -Format "MMddyyyy-HH:mm:ss").log"

If (-not (Test-Path -Path $script:Logpath -PathType Container) )
{
    New-Item -Path $script:Logpath -ItemType Directory -Force
}



enum CASSettings
{
    Default
    MobileOnly
    DesktopOnly
    OWAOnly
    DesktopOWA
    TermPending
}

function ResetTest
{
<#
.SYNOPSIS
    Reset account to defaults.

.DESCRIPTION
    Used for testing and resets the user account to defaults by removing associations to mobile devices and sets the client access settings to company defaults.

.EXAMPLE
    ResetTest "thinlizzy@sunriserecords.com"

#>
    param
    (
        [Parameter(Mandatory=$true)][String]$UserPrincipalName
    )

    [void](Check-ServiceSession)

    Set-QuickCASMailbox -AccessSetting ([CASSettings]::DesktopOWA) -UserPrincipalName $UserPrincipalName
    Get-MobileDevice –Mailbox $UserPrincipalName | ForEach-Object { Remove-MobileDevice $_.DistinguishedName -Confirm:$false }
    Get-QuickCASMailbox $UserPrincipalName
}
Function Start-Logging{
    Start-transcript -path $script:LogFile -Force
}
Function Stop-Logging{
    Stop-Transcript
}

Get-ChildItem -Path $PSSCriptRoot\Public  -Filter *-*.ps1 | ForEach-Object {. $_.FullName }
Get-ChildItem -Path $PSSCriptRoot\Private  -Filter *-*.ps1 | ForEach-Object {. $_.FullName }

Export-ModuleMember -Variable CASSetting
Export-ModuleMember -Variable CompanyDomain
Export-ModuleMember -Variable Office365Session