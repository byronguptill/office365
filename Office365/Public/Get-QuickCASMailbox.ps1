function Get-QuickCASMailbox
{
    param
    (
        [Parameter(Mandatory=$true)][String]$UserPrincipalName,
        [String]
        $Cr,
        [String]
        $AppID,
        [String]
        $Organization,
        [String]
        $CertificateThumbprint   
    )
<#
.SYNOPSIS
    Show the protocol status for the email access.

.DESCRIPTION
    Use this function to view a list of the protocols that are used to access email and their associated status.

.EXAMPLE
    Get-QuickCASMailbox "thinlizzy@sunriserecords.com"

#>
    param([Parameter(Mandatory=$true)][String]$UserPrincipalName)

    if ($PSBoundParameters.ContainsKey('Cr')) 
    {
        [void](Connect-ServiceSession $Cr)
    }
    elseif ($PSBoundParameters.ContainsKey('AppID') -and $PSBoundParameters.ContainsKey('Organization') -and $PSBoundParameters.ContainsKey('CertificateThumbprint') ) 
    {
        [void](Connect-ServiceSession -AppID $global:app_id -Organization $global:tenant_id -CertificateThumbprint $global:CertificateThumbprint)
    } else 
    {
        [void](Connect-ServiceSession)
    }

    Get-CASMailbox -Identity (Get-Mailbox $UserPrincipalName).DistinguishedName | `
                    Select ActiveSyncEnabled, `
                    OWAEnabled, `
                    PopEnabled, `
                    ImapEnabled, `
                    MAPIEnabled, `
                    MAPIBlockOutlookExternalConnectivity, `
                    UniversalOutlookEnabled, `
                    OutlookMobileEnabled, `
                    EwsEnabled, `
                    EwsAllowEntourage, `
                    EwsAllowMacOutlook, `
                    EwsAllowOutlook, `
                    EWSBlockList, `
                    EWSAllowList
}
