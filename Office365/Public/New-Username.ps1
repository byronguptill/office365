function New-Username 
{
<#
.SYNOPSIS
    Create a unique username.

.DESCRIPTION
    A longer description.

.PARAMETER GivenName
    The first name of the new user.

.PARAMETER Surname
    The family name of the new user.

.EXAMPLE
    Example of how to run the script.

#>
    param 
    (
        [Parameter(Mandatory=$true)]
        [String]
        $GivenName,
        [Parameter(Mandatory=$true)]
        [String]
        $Surname,
        [Parameter(Mandatory=$false)]
        [String]
        $Cr,
        [String]
        $AppID,
        [String]
        $Organization,
        [String]
        $CertificateThumbprint   

    )

    begin {
        if ( $PSBoundParameters.ContainsKey('Cr') )
        {
            [void](Connect-ServiceSession $Cr)
        }
        elseif ($PSBoundParameters.ContainsKey('AppID') -and $PSBoundParameters.ContainsKey('Organization') -and $PSBoundParameters.ContainsKey('CertificateThumbprint') ) {
            [void](Connect-ServiceSession -AppID $global:app_id -Organization $global:tenant_id -CertificateThumbprint $global:CertificateThumbprint)
        }
        else 
        {
            [void](Connect-ServiceSession)
        }

        $GivenName_temp = $GivenName.ToCharArray()
        $UsernamePrefix, $GivenName_temp = $GivenName_temp
        $SuggestedUsername = [string]$UsernamePrefix +  $Surname.Replace(" ","")
    }

    
    process {
        while ($GivenName_temp)
        {
            $object = Get-AzureADUser -Filter "mailnickname eq '$SuggestedUsername'"
            if ( $object ) 
            {
                $first, $GivenName_temp = $GivenName_temp
                $UsernamePrefix = $UsernamePrefix + [string]$first
                $SuggestedUsername = $UsernamePrefix +  $Surname
            } 
            else 
            {
                break    
            }
            if ($GivenName_temp.Count -eq 0 -and $null -ne $object) {
                throw "Could not determine username for the $GivenName $Surname"
            }

        }    
    }

    end {
        $SuggestedUsername.ToLower().trim()
    }
}