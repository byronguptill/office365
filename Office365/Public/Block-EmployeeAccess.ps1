function Block-EmployeeAccess
{
<#
.SYNOPSIS
    Blocks access to email.

.DESCRIPTION
    Sets Client Access Settings to prevent access to Office 365 email and sends a wipe request to the mobile devices that are associated with the users' mail account.

.EXAMPLE
    Block-EmployeeAccess "thinlizzy@everesttoys.com"

.NOTES
    This fuction should be used in advance of disabling or changing the password of the employee that is to be terminated.  Upon notification of the mobile device acknowledgement the user account can be disabled.

#>
    param
    (
        [Parameter(Mandatory=$true)][String]$UserPrincipalName,

        [String]
        $Cr,
        [String]
        $AppID,
        [String]
        $Organization,
        [String]
        $CertificateThumbprint   
    )

    begin {
        if ($PSBoundParameters.ContainsKey('Cr')) 
        {
            [void](Connect-ServiceSession $Cr)
        }
        elseif ($PSBoundParameters.ContainsKey('AppID') -and $PSBoundParameters.ContainsKey('Organization') -and $PSBoundParameters.ContainsKey('CertificateThumbprint') ) 
        {
            [void](Connect-ServiceSession -AppID $global:app_id -Organization $global:tenant_id -CertificateThumbprint $global:CertificateThumbprint)
        } else 
        {
            [void](Connect-ServiceSession)
        }
        
        $notification_email = "technologyservices@everesttoys.com"
    }
    process {
        Set-QuickCASMailbox -AccessSetting ([CASSettings]::TermPending) -UserPrincipalName $UserPrincipalName
        Get-MobileDevice -Mailbox $UserPrincipalName | ForEach-Object { Clear-MobileDevice -Identity $_.DistinguishedName -AccountOnly -NotificationEmailAddresses $notification_email -Confirm:$false }
        # remove on-premise account from VPN group.  disable on-premise account.
    }
}
