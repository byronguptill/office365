function New-Employee {
    <#
        .SYNOPSIS
            Creates a new user account in Azure AD.
    
        .DESCRIPTION
            Creates a new user account in Azure AD and allows you to specify the Office 365 licenses that need to be associated to the new user..
    
        .INPUTS
            Does not accept piped objects.
    
        .EXAMPLE
    $Company = ([CompanyDomain]::SunriseRecords)
    $PasswordAsPlainText = [System.Web.Security.Membership]::GeneratePassword(14,3)
    $password = ConvertTo-SecureString $PasswordAsPlainText -AsPlainText -Force
    $GivenName = "Led"
    $Surname = "Zepplin"
    $Title = "Musician"
    $Department = "Music Legends"
    $ManagerUPN = "proque@everesttoys.com"
    
    
    $AzureUserAccount = New-Employee -Company $Company -GivenName $GivenName -Surname $Surname -Password $password -Title $title -Department $Department -ManagerUPN $ManagerUPN
    
    
        .LINK
    
        .NOTES
            
    #>
    [CmdletBinding(SupportsShouldProcess=$true,ConfirmImpact='low')]
    param 
    (
        # Specify the domain the user will belong to.  I.E "everesttoys.com" or "sunriserecords.com"
    
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [ArgumentCompleter({ $global:ValidCompany.Keys })]
        [ValidateScript({$_ -in $Global:ValidCompany.Keys })]
        [String]$Company,
        
        # The first name of the user
    
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]$GivenName,
    
        # The family name of the user
    
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]$Surname,
    
        # The name that should appear in Global Address Llist and Outlook
    
        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        [String]$DisplayName,
    
        # The job title for the user account
    
        [Parameter(Mandatory=$True)]
        [ValidateNotNullOrEmpty()]
        [String]$Title,
    
        # The password for the user account
    
        [Parameter(Mandatory=$True)]
        [ValidateNotNullOrEmpty()]
        [System.Security.SecureString]$Password,
    
        # The desired account name for the user  
    
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]$MailNickName,
    
        # The department the user will belong to
    
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]$Department,
    
    
        # The manager the user will be reporting to
    
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]$ManagerUPN,
    
        # Specify this switch add the user to the licensing group for Office 365 Business Essentials
    
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [ValidateSet("E3","Essentials")]
        [String]$Office365Subscription,
    
        [Parameter(Mandatory=$false)]
        [String]$GroupMemberships,
    
        [String]
        $Cr,

        [String]
        $Intune,

        [Switch]
        $MFA,
        [String]
        $AppID,
        [String]
        $Organization,
        [String]
        $CertificateThumbprint   
    )
        
    begin {
        if ($PSBoundParameters.ContainsKey('Cr')) 
        {
            [void](Connect-ServiceSession $Cr)
        }
        elseif ($PSBoundParameters.ContainsKey('AppID') -and $PSBoundParameters.ContainsKey('Organization') -and $PSBoundParameters.ContainsKey('CertificateThumbprint') ) 
        {
            [void](Connect-ServiceSession -AppID $global:app_id -Organization $global:tenant_id -CertificateThumbprint $global:CertificateThumbprint)
        } else 
        {
            [void](Connect-ServiceSession)
        }

        $Domain = $global:ValidCompany[$Company]

        $BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($Password)
        $UnsecurePassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)

        #region set password
        $PasswordProfile = New-Object -TypeName Microsoft.Open.AzureAD.Model.PasswordProfile
        $PasswordProfile.Password = $UnsecurePassword
        Write-MyLog "$($MyInvocation.MyCommand)`t`tPassword has been set to: [redacted]" 
        #endregion
        
        #region get manager
        Write-MyLog "$($MyInvocation.MyCommand)`t`tSearching for the manager object using: $ManagerUPN" 

        $manager = Get-AzureAdUser -Filter "UserPrincipalName eq '$ManagerUPN'"

        if ($manager.Count -lt 1) { throw "Manager not found"}
        Write-MyLog "*** Found: $($manager.DisplayName)" 
        #endregion 
        
        if ($PSBoundParameters.ContainsKey("GroupMemberships"))
        {
            Write-MyLog "$($MyInvocation.MyCommand)`t`GroupMemberships: $GroupMemberships"
            $Groups = $GroupMemberships.Split("|") 
        }
        else 
        {
            Write-MyLog "$($MyInvocation.MyCommand)`t`GroupMemberships: **Not Set**"
        }
        
        $UPN = -join ($MailNickName,"@",$Domain)
        if (-not $DisplayName)
        {
            $DisplayName = "$GivenName $Surname"
        }
        #endregion   
        $newuser = [PSCustomObject] @{ 
            Domain = $Domain
            'Given Name' = $GivenName
            Surname = $Surname
            'Display name' = $DisplayName
            Title = $Title
            Password = $Password
            'Mail nickname' = $MailNickName
            Deplartment = $Department
            'User principal name' = $UPN
            'Manager nickname' = $ManagerMailNickName
            'Manager user principal name' = $ManagerUPN
        } 
        Write-MyLog  "$($MyInvocation.MyCommand)`t`t $newuser"
    }
    
    process {
        #if ($PSCmdlet.ShouldProcess($GivenName, 'Connfirm create user action in Azure AD.'))
        if ($PSCmdlet.ShouldProcess("Office 365 Tenant", 'Create Employee'))
        {   
            $response = New-AzureADUser -GivenName $GivenName `
                                        -Surname $Surname `
                                        -Department $Department `
                                        -AccountEnabled $True `
                                        -DisplayName $DisplayName `
                                        -PasswordProfile $PasswordProfile `
                                        -MailNickName $MailNickName `
                                        -UserPrincipalName $upn `
                                        -JobTitle $Title
            Set-AzureADUserManager -ObjectId $response.ObjectId -RefObjectId $manager.ObjectId    

            Write-MyLog  "$($MyInvocation.MyCommand)`t`t New user objectid: $($response.ObjectId)"

            #Add-RecipientPermission -Identity $response.UserPrincipalName -Trustee webadmin@everesttoys.com -AccessRights SendAs -Confirm:$false -Verbose

            if ($Office365Subscription -match "E3") { Add-AzureADGroupMember -ObjectId "f6f0eb0c-c630-4f93-b665-37fedcc32a4a" -RefObjectId $response.ObjectId}
            else { Add-AzureADGroupMember -ObjectId "544e61d4-c7e4-4e60-af18-e31f8698dc67" -RefObjectId $response.ObjectId}
            if ($Intune -eq "Yes") 
            { 
                Add-AzureADGroupMember -ObjectId "a6775edd-77bb-4e44-8739-57bc6eb29053" -RefObjectId $response.ObjectId
                # Set-QuickCASMailbox -AccessSetting Default -UserPrincipalName $UPN  
            }
            foreach ($group in $Groups)
            {
                try {
                    Add-AzureADGroupMember -ObjectID $(Get-AzureADGroup -SearchString $group).ObjectID -RefObjectId $response.ObjectId
                } catch {
                    
                }
            }
            if ($MFA) 
            {
                try 
                {

                    Start-Sleep -Seconds 120

                    Connect-MSOLService -Credential $Script:UserCredential

                    Write-MyLog  "$($MyInvocation.MyCommand)`t`t Set MFA swiitch for: $($response.UserPrincipalName)"

                    $mf = New-Object -TypeName Microsoft.Online.Administration.StrongAuthenticationRequirement
                    $mf.RelyingParty = "*"
                    $mf.State = "Enabled"
                    $mfarr = @($mf)
                } 
                catch 
                {
                    Write-MyLog "unable to set the 2FA.  $_"

                }
                try {
                    Write-MyLog  "$($MyInvocation.MyCommand)`t`t Execute get and set-msol on: $($response.UserPrincipalName)"
                    Get-Msoluser -UserPrincipalName $response.UserPrincipalName | set-msoluser -StrongAuthenticationRequirements $mfarr                         
                } catch  {
                    Write-MyLog  "$($MyInvocation.MyCommand)`t`t $($error[0])"
                }
            }
        }  
    }
    
    end {
        $response
    }
}