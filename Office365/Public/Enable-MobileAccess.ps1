function Enable-MobileAccess 
{
<#
.SYNOPSIS
    Enable mobile device access to email account.

.DESCRIPTION
    Sets the values to true for EwsEnabled and OutlookMobileEnabled.

.EXAMPLE
    Enable-MobileAccess "thinlizzy@everesttoys.com"

#>
    param
    (
        [Parameter(Mandatory=$true)][String]$UserPrincipalName,
        [String]
        $Cr,
        [String]
        $AppID,
        [String]
        $Organization,
        [String]
        $CertificateThumbprint   
    )
    
    if ($PSBoundParameters.ContainsKey('Cr')) 
    {
        [void](Connect-ServiceSession $Cr)
    }
    elseif ($PSBoundParameters.ContainsKey('AppID') -and $PSBoundParameters.ContainsKey('Organization') -and $PSBoundParameters.ContainsKey('CertificateThumbprint') ) 
    {
        [void](Connect-ServiceSession -AppID $global:app_id -Organization $global:tenant_id -CertificateThumbprint $global:CertificateThumbprint)
    } else 
    {
        [void](Connect-ServiceSession)
    }

    Set-CASMailbox -Identity (Get-Mailbox $UserPrincipalName).DistinguishedName `
                -OutlookMobileEnabled $true `
                -EwsEnabled $true

    $response = Get-AzureADUser -Filter "UserPrincipalName eq '$UserPrincipalName'"
    if (-not (Get-AzureADGroupMember -ObjectId "a6775edd-77bb-4e44-8739-57bc6eb29053").Contains($response))
    {
        Add-AzureADGroupMember -ObjectId "a6775edd-77bb-4e44-8739-57bc6eb29053" -RefObjectId $response.ObjectId
        Write-Host "The user has been assigned an Intune License."
    }
    else 
    {
        Write-Host "The user is already a member of the Intune License Assignment Group."
    }
}