function Get-AzureADUserDetails
{
<#
.SYNOPSIS
    Output summary of user account.

.DESCRIPTION
    This function is to be used to verify if an account has been setup properly.

.EXAMPLE
     Get-AzureADUserDetails  ("jessica@everesttoys.com","danielle@sunriserecords.com","niko@sunriserecords.com","pat@sunriserecords.com")

#>
    param
    (
        [Parameter(Mandatory=$true)][Object]$Objects
    )

    Foreach ($object in $Objects)
    {
        $user = Get-AzureADUser -ObjectId $object
        $Intune = Test-GroupMembership -Reference (Get-AzureADGroupMember -ObjectId "a6775edd-77bb-4e44-8739-57bc6eb29053") -Comparison $user #Intune
        $E3 = Test-GroupMembership -Reference (Get-AzureADGroupMember -ObjectId "f6f0eb0c-c630-4f93-b665-37fedcc32a4a")  -Comparison $user #E3
        $Essentials = Test-GroupMembership -Reference (Get-AzureADGroupMember -ObjectId "544e61d4-c7e4-4e60-af18-e31f8698dc67") -Comparison $user #Essentials
        try 
        {
            $manager = Get-AzureADUserManager -ObjectId $user.ObjectId
        }
        catch 
        {
            $manager = ""
        }
   
        $temp = @{
           firstname = $user.GivenName
           lastname = $user.Surname
           title = $user.JobTitle
           displayname = $user.DisplayName
           MailNickName = $user.MailNickName
           upn = $user.UserPrincipalName
           mail = $user.Mail
           department = $user.Department
           enabled = $user.AccountEnabled
           intune = $Intune
           e3 = $E3
           essentials = $Essentials
           manager = $manager.DisplayName
        }

        write-host `
            "`n`tGiven Name: " $temp.firstname `
            "`n`tSurename: " $temp.lastname `
            "`n`tTitle: " $temp.title `
            "`n`tDisplay Name: " $temp.displayname `
            "`n`tMailNickName: " $temp.MailNickName `
            "`n`tUse Principle Name: " $temp.upn `
            "`n`tMail: " $temp.mail `
            "`n`tDepartment: " $temp.department `
            "`n`tEnabled: " $temp.enabled `
            "`n`tE3 License: " $temp.e3 `
            "`n`tEssentials: " $temp.essentials `
            "`n`tIntune License: " $temp.intune `
            "`n`tManager: " $temp.manager
    }
}  