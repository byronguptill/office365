function Set-QuickCASMailbox
{
<#
.SYNOPSIS
    Set the client access settings for the user account.

.DESCRIPTION
    Set the client access settings for the user account to Default, mobile only, desktop only, owa only, desktop and owa, or termination pending.

.EXAMPLE
    Set-QuickCASMailbox -AccessSetting ([CASSettings]::TermPending) -UserPrincipalName "karley@everesttoys.com"

#>
    [CmdletBinding()]
    param
    (
        [ValidateSet("TermPending","DesktopOWA","OWAOnly","DesktopOnly","MobileOnly","Default")]
        [Parameter(Mandatory=$true)]
        [String]
        $AccessSetting,

        [Parameter(Mandatory=$true)]
        [String]
        $UserPrincipalName,

        [String]
        $Cr,
        [String]
        $AppID,
        [String]
        $Organization,
        [String]
        $CertificateThumbprint   
    )
    begin{

        if ($PSBoundParameters.ContainsKey('Cr')) 
        {
            [void](Connect-ServiceSession $Cr)
        }
        elseif ($PSBoundParameters.ContainsKey('AppID') -and $PSBoundParameters.ContainsKey('Organization') -and $PSBoundParameters.ContainsKey('CertificateThumbprint') ) 
        {
            [void](Connect-ServiceSession -AppID $global:app_id -Organization $global:tenant_id -CertificateThumbprint $global:CertificateThumbprint)
        } else 
        {
            [void](Connect-ServiceSession)
        }

        try {
            $mailbox = Get-Mailbox $UserPrincipalName
        } catch {
            throw $_
        }

    }

    process {
        Write-Verbose "Setting the Client Access Setting to Everest Toys $AccessSetting setting for account $UserPrincipalName."

        switch ($AccessSetting)
        {
            MobileOnly 
            {
                Set-CASMailbox -Identity $mailbox.DistinguishedName `
                        -ActiveSyncEnabled $false `
                        -OWAEnabled $false `
                        -PopEnabled $false `
                        -ImapEnabled $false `
                        -MAPIEnabled $false `
                        -OWAforDevicesEnabled $false `
                        -UniversalOutlookEnabled $false `
                        -OutlookMobileEnabled $true `
                        -EwsEnabled $true `
                        -EwsAllowList @{add="Microsoft Office/*", "Microsoft+Office/*", "UCCAPI/*", "OC/*", "Mozilla/*","Outlook-iOS/*","Outlook-Android/*", "*Teams/*","UCWA/*","LogicAppsDesigner/*","azure-logic-apps/*","PowerApps/*"}
                        
            }
            DesktopOnly
            {
                Set-CASMailbox -Identity $mailbox.DistinguishedName `
                        -ActiveSyncEnabled $false `
                        -OWAEnabled $false `
                        -PopEnabled $false `
                        -ImapEnabled $false `
                        -MAPIEnabled $true `
                        -OWAforDevicesEnabled $false `
                        -UniversalOutlookEnabled $false `
                        -OutlookMobileEnabled $false `
                        -EwsEnabled $true `
                        -EwsAllowList @{add="Microsoft Office/*", "Microsoft+Office/*", "UCCAPI/*", "OC/*", "Mozilla/*","Outlook-iOS/*","Outlook-Android/*", "*Teams/*","UCWA/*"}
            }
            OWAOnly
            {
                Set-CASMailbox -Identity $mailbox.DistinguishedName `
                        -ActiveSyncEnabled $false `
                        -OWAEnabled $true `
                        -PopEnabled $false `
                        -ImapEnabled $false `
                        -MAPIEnabled $false `
                        -OWAforDevicesEnabled $false `
                        -UniversalOutlookEnabled $false `
                        -OutlookMobileEnabled $false `
                        -EwsEnabled $false `
                        -EwsAllowList @{add="Microsoft Office/*", "Microsoft+Office/*", "UCCAPI/*", "OC/*", "Mozilla/*","Outlook-iOS/*","Outlook-Android/*", "*Teams/*","UCWA/*"}
            }
            DesktopOWA
            {
                Set-CASMailbox -Identity $mailbox.DistinguishedName `
                        -ActiveSyncEnabled $false `
                        -OWAEnabled $true `
                        -PopEnabled $false `
                        -ImapEnabled $false `
                        -MAPIEnabled $true `
                        -OWAforDevicesEnabled $false `
                        -UniversalOutlookEnabled $false `
                        -OutlookMobileEnabled $false `
                        -EwsEnabled $true `
                        -EwsAllowList @{add="Microsoft Office/*", "Microsoft+Office/*", "UCCAPI/*", "OC/*", "Mozilla/*","Outlook-iOS/*","Outlook-Android/*", "*Teams/*","UCWA/*"}
            }
            TermPending
            {
                Set-CASMailbox -Identity $mailbox.DistinguishedName `
                        -ActiveSyncEnabled $false `
                        -OWAEnabled $false `
                        -PopEnabled $false `
                        -ImapEnabled $false `
                        -MAPIEnabled $false `
                        -OWAforDevicesEnabled $false `
                        -UniversalOutlookEnabled $false `
                        -OutlookMobileEnabled $true `
                        -EwsEnabled $true `
                        -EwsAllowList @{add="Microsoft Office/*", "Microsoft+Office/*", "UCCAPI/*", "OC/*", "Mozilla/*","Outlook-iOS/*","Outlook-Android/*", "*Teams/*","UCWA/*"}
            }
            Default 
            {
                Set-CASMailbox -Identity $mailbox.DistinguishedName `
                            -ActiveSyncEnabled $false `
                            -OWAEnabled $true `
                            -PopEnabled $false `
                            -ImapEnabled $false `
                            -MAPIEnabled $true `
                            -OWAforDevicesEnabled $false `
                            -UniversalOutlookEnabled $false `
                            -OutlookMobileEnabled $true `
                            -EwsEnabled $true `
                            -EwsAllowEntourage $false `
                            -EwsAllowMacOutlook $true `
                            -EwsAllowOutlook $true `
                            -EwsAllowList @{add="Microsoft Office/*", "Microsoft+Office/*", "UCCAPI/*", "OC/*", "Mozilla/*","Outlook-iOS/*","Outlook-Android/*", "*Teams/*", "UCWA/*"}

            }
        }
    }

    end {

    }
}