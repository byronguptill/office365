
function Get-QuickMailboxStatistics
{
    param (
        [String]
        $AppID,
        [String]
        $Organization,
        [String]
        $CertificateThumbprint   
    )
<#
.SYNOPSIS
    Show the status of the mobile devices associated to the mail account.

.DESCRIPTION
    Use this function to view the device wipe status and whether the device(s) have acknowledged the kill signal.

.EXAMPLE
    Get-QuickCASMailbox "thinlizzy@sunriserecords.com"

#>
    param([Parameter(Mandatory=$true)][String]$UserPrincipalName)

    if ($PSBoundParameters.ContainsKey('AppID') -and $PSBoundParameters.ContainsKey('Organization') -and $PSBoundParameters.ContainsKey('CertificateThumbprint') ) 
    {
        [void](Connect-ServiceSession -AppID $global:app_id -Organization $global:tenant_id -CertificateThumbprint $global:CertificateThumbprint)
    } else 
    {
        [void](Connect-ServiceSession)
    }

    Get-MobileDeviceStatistics -Mailbox $UserPrincipalName | select DeviceUserAgent, *wipe*
}
