function Get-AzureADUserDetail
{
<#
.SYNOPSIS
    Output summary of user account.

.DESCRIPTION
    This function is to be used to verify if an account has been setup properly.

.EXAMPLE
     Get-AzureADUserDetails  ("jessica@everesttoys.com","danielle@sunriserecords.com","niko@sunriserecords.com","pat@sunriserecords.com")

#>
    [CmdletBinding()]
    param
    (
        [Parameter(Mandatory=$false)]
        [ValidatePattern("\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b")]
        [String[]]
        $UserPrincipalName,

        [Parameter(Mandatory=$false)]
        [ArgumentCompleter({ $global:ValidCompany.Keys })]
        [ValidateScript({$_ -in $Global:ValidCompany.Keys })]
        [String]
        $Company,

        [Parameter(Mandatory=$false)]
        [ValidateSet("E3","BusinessEssentials")]
        [String]
        $License,

        [String]
        $Cr,
        [String]
        $AppID,
        [String]
        $Organization,
        [String]
        $CertificateThumbprint   
    )

    begin {
        
        if ($PSBoundParameters.ContainsKey('Cr')) 
        {
            [void](Connect-ServiceSession $Cr)
        }
        elseif ($PSBoundParameters.ContainsKey('AppID') -and $PSBoundParameters.ContainsKey('Organization') -and $PSBoundParameters.ContainsKey('CertificateThumbprint') ) 
        {
            [void](Connect-ServiceSession -AppID $global:app_id -Organization $global:tenant_id -CertificateThumbprint $global:CertificateThumbprint)
        } else 
        {
            [void](Connect-ServiceSession)
        }

        [object]$AzureADUser = New-Object System.Collections.ArrayList
        if ($PSBoundParameters.ContainsKey('UserPrincipalName'))
        {
            $object = New-Object System.Collections.ArrayList
            foreach ($principal in $UserPrincipalName)
            {
                Write-Verbose "[VERBOSE]`tQuery for user $user"
                [void]$object.Add($(Get-AzureADUser -ObjectId $principal))
            }
        }
        else
        {
                $object = Get-AzureADUser -All $true
        }

        if  ($PSBoundParameters.ContainsKey('Company'))
        {
            switch ($Company)
            {
                SunriseRecords { $Domain = "sunriserecords.com" }
                EverestToys { $Domain = "everesttoys.com" }
            }
            $object = Get-AzureADUser -All $true | Where-Object { $_.UserPrincipalName -match $Domain -and $_.AccountEnabled -eq $true}
        }

        

    }

    process {

        $userrank = 0
        Foreach ($user in $object)
        {            
            $percent_complete = ($userrank++)/$object.count*100
            Write-Progress -Activity "Processing user $($user.DisplayName)" -PercentComplete $percent_complete -Status "$userrank accounts processed of $($object.count)"
            $Intune = Test-GroupMembership -Reference (Get-AzureADGroupMember -ObjectId "a6775edd-77bb-4e44-8739-57bc6eb29053") -Comparison $user #Intune
            Write-Verbose "[VERBOSE]`tTesting membership to Intune License group: $Intune"  

            $E3 = Test-GroupMembership -Reference (Get-AzureADGroupMember -ObjectId "f6f0eb0c-c630-4f93-b665-37fedcc32a4a")  -Comparison $user #E3
            Write-Verbose "[VERBOSE]`tTesting membership to E3 License group: $E3"  
<#
            $Essentials = Test-GroupMembership -Reference (Get-AzureADGroupMember -ObjectId "544e61d4-c7e4-4e60-af18-e31f8698dc67") -Comparison $user #Essentials
            Write-Verbose "[VERBOSE]`tTesting membership to Essentials License group: $Intune"  
#>
            try 
            {
                $manager = Get-AzureADUserManager -ObjectId $user.ObjectId
                Write-Verbose "[VERBOSE]`tManager is: $($manager.DisplayName)"  
            }
            catch 
            {
                $manager = ""
            }

            $AzureAdObject = [PSCustomObject]@{
                ObjectId = $user.ObjectId
                'FirstName' = $user.GivenName
                'LastName' = $user.Surname
                Title = $user.JobTitle
                'DisplayName' = $user.DisplayName
                "MailNickName" = $user.MailNickName
                "UserPrincipalName" = $user.UserPrincipalName
                "Mail" = $user.Mail
                Department = $user.Department
                Enabled = $user.AccountEnabled
                Intune = $Intune
                E3 = $E3
                Essentials = $Essentials
                Manager = $manager.DisplayName                 
            }
            [void]$AzureADUser.Add($AzureAdObject)
        } 
    }
    end {
        $AzureADUser
    }
}
