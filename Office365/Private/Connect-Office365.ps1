function Connect-Office365
{
    param(
        [System.Management.Automation.PSCredential]
        $Credential =[System.Management.Automation.PSCredential]::Empty,
        [String]
        $AppID,
        [String]
        $Organization,
        [String]
        $CertificateThumbprint
    )
    
    $connected = $true

    try 
    {
        get-mailbox
    } 
    catch [System.Management.Automation.CommandNotFoundException] 
    {
        write-output "Was not able to query Office 365 Tenant, perhaps there is no active connection."
        $connected = $false
    } 
    catch 
    {
        write-output "An error occured that could not be resolved."
    }

    if ( -not $connected )
    {
        if ($PSBoundParameters.ContainsKey('AppID') -and $PSBoundParameters.ContainsKey('Organization') -and $PSBoundParameters.ContainsKey('CertificateThumbprint') )
        {
            $script:AppID = $AppID 
            $script:Organization = $Organization
            $script:CertificateThumbprint = $CertificateThumbprint
        } 
        elseif ($PSBoundParameters.ContainsKey('Credential'))
        {
            $script:Credential = $Credential
        }
        else 
        {
            $script:Credential = Get-Credential
        }
    } 

    if ($script:Credential) 
    {
        Connect-ExchangeOnline -Credential $script:Credential 
    }
    else 
    {
        Connect-ExchangeOnline -CertificateThumbprint $script:CertificateThumbprint -AppID $script:AppID -Organization $script:Organization
    }
}