function Write-MyLog {
    [CmdletBinding()]
    param (

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]$Message

    )
    begin {
    }
    process {
        Write-Verbose $Message
        Write-EventLog -LogName Application -Source Office365 -EventId 65002 -EntryType Information -Message $Message

    }
    end {

    }
}