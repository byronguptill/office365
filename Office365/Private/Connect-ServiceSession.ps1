function Connect-ServiceSession
{
    [CmdletBinding()]
    param (
        [String]
        $Cr,
        [String]
        $AppID,
        [String]
        $Organization,
        [String]
        $CertificateThumbprint        
    )
    begin{

        $ConnectionType = 0

        if ( $PSBoundParameters.ContainsKey('Cr') )
        {
            $creds = $(Get-Content $Cr).Split("`n")
            Write-Debug $creds[0]
            Write-Debug $creds[1]
            $script:UserCredential = New-Object System.Management.Automation.PSCredential -ArgumentList $creds[0], ($creds[1] | ConvertTo-SecureString)
        }
        elseif ($PSBoundParameters.ContainsKey('AppID') -and $PSBoundParameters.ContainsKey('Organization') -and $PSBoundParameters.ContainsKey('CertificateThumbprint') ) {
            $ConnectionType = 1
        }
        else {
            $script:UserCredential = Get-Credential
        }
    }
    process{
        switch ($ConnectionType)
        {
            0 {
                if ($script:Office365Session.State -ne "Opened")
                {
                    Connect-Office365 $script:UserCredential
                }
                try 
                {
                    Get-AzureADTenantDetail 
                } 
                catch [Microsoft.Open.Azure.AD.CommonLibrary.AadNeedAuthenticationException] 
                {
                    Connect-AzureAD -Credential $script:UserCredential 
                }
            }
            1 {
                if ($script:Office365Session.State -ne "Opened")
                {
                    Connect-Office365 -TenantId $Organization -ApplicationId $AppID -CertificateThumbprint $CertificateThumbprint
                }
                try 
                {
                    Get-AzureADTenantDetail 
                } 
                catch [Microsoft.Open.Azure.AD.CommonLibrary.AadNeedAuthenticationException] 
                {
                    Connect-AzureAD -TenantId $Organization -ApplicationId $AppID -CertificateThumbprint $CertificateThumbprint
                }
            }
        }
    }
    End
    {
    }
}


#Connect-ServiceSession -Cr '\\everest-sts01\c$\temp\newhire\nhcredentials.txt'

#Connect-ServiceSession -Cr "C:\Users\byron\OneDrive - Everest Toys\Documents\source\password.txt"