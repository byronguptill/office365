Function Test-GroupMembership
{
<#
.SYNOPSIS
    Test the membership of a user to a group.

.DESCRIPTION
    The command Get-AzureADGroupMember returns an array of Directory Objects or a Directory Object.  We need to have a function that is flexible to test existance in an array or equivalance if a single object is returned.

.EXAMPLE
    $Reference = (Get-AzureADGroupMember -ObjectId "544e61d4-c7e4-4e60-af18-e31f8698dc67")
    $Reference = (Get-AzureADGroupMember -ObjectId "f6f0eb0c-c630-4f93-b665-37fedcc32a4a")
    $Comparison = (Get-AzureADUser -ObjectId "byron@everesttoys.com")
    Test-GroupMembership -Reference $reference -Comparison $comparison

#>
    param
    (
        [Parameter(Mandatory=$true)][Object]$Reference,
        [Parameter(Mandatory=$true)][Object]$Comparison
    )
    $_referenceType = $Reference.GetType()
    if ( $_referenceType.BaseType -eq [System.Array] )
    {
        return $Reference.contains($comparison)
    }
    else
    {
        return $Reference.Equals($Comparison)
    }
 }