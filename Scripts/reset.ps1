Write-Output "`nRemove existing PSSessions.`n"
Get-PSSession | Remove-PSSession
Get-PSSession
Write-Output "`nChecking for modules that are loaded.`n"
Get-Module | ? {$_.Name -match "Office" -or $_.Name -match "Eve"}
Remove-Module EveAdmin -Force -ErrorAction SilentlyContinue | Out-Null
Remove-Module Office365 -Force -ErrorAction SilentlyContinue | Out-Null
Write-Output "`nVerify that modules that are unloaded.`n"
. "$PSScriptRoot\load.ps1"
Write-Output "`nChecking for modules that are loaded.`n"
Get-Module | ? {$_.Name -match "Office" -or $_.Name -match "Eve"}